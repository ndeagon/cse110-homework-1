package edu.ucsd.cse110.library.rules;

import edu.ucsd.cse110.library.MemberType;


public class StudentLateSchoolYearAssessor implements Assessor {

	
	public boolean evaluate(Properties prop) {
		//During the winter students can keep books for 2 weeks
		if (prop.getType() == MemberType.Student &&
				prop.getCheckoutDate().getMonthValue()<=6 &&
					prop.getCheckoutDate().getMonthValue()>=9) 
				return prop.getDays()>14;
		return false;
	}

	
	public String getErrors() {
		return null;
	}

}
