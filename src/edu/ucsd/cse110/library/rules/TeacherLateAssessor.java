package edu.ucsd.cse110.library.rules;

import edu.ucsd.cse110.library.MemberType;


public class TeacherLateAssessor implements Assessor {

	
	public boolean evaluate(Properties prop) {
		return prop.getType() == MemberType.Teacher 
				&& prop.getDays()>14;
	}

	
	public String getErrors() {
		return null;
	}

}
