package edu.ucsd.cse110.library;

import java.util.ArrayList;

public class Library {
	
	ArrayList<Member> members = new ArrayList<Member>();
	ArrayList<Publication> pubs = new ArrayList<Publication>();
	
	factory factory = new factory();
		
	FacadeInterface checker = factory.make("enterpriseComponent");
	
	
	public void checkOut(Member m, Publication p){
		checker.checkoutPublication(m, p);
	}
	
	public void returnPublication(Publication p){
		checker.returnPublication(p);
	}
	
	public void checkFees(Member m){
		checker.getFee(m);
	}
	
	public void hasFees(Member m){
		checker.hasFee(m);
	}
	
	public void addMember(Member m){
		members.add(m);
	}
	
	public void removeMember(Member m){
		members.remove(m);
	}
	
	public void addPublication(Publication p){
		pubs.add(p);
	}
	
	public void removePublication(Publication p){
		pubs.remove(p);
	}

}
