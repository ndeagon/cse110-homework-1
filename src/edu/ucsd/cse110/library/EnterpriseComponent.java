package edu.ucsd.cse110.library;

import java.time.LocalDate;

public class EnterpriseComponent implements FacadeInterface
{
	public void checkoutPublication(Member m, Publication p) {
		LocalDate checkoutDate = LocalDate.of(2015, 2, 5); // using this checkout date to test latefees
		                                                   // otherwise use "now()"
		p.checkout(m, checkoutDate);
		m.checkingOut(p);
	}

	public void returnPublication(Publication p) {
		LocalDate returnDate = LocalDate.of(2015, 2, 21); // using this return date to test latefees
		                                                  // otherwise use "now()"
		p.getMember().returning(p);
		p.pubReturn(returnDate);

	}

	public double getFee(Member m) {
		return m.getDueFees();
	}

	public boolean hasFee(Member m) {
		return m.getDueFees() > 0;
	}
	
	public boolean isCheckedOut(Publication p) {
		return p.isCheckout();
	}

}
