package edu.ucsd.cse110.library;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.time.LocalDate;

import edu.ucsd.cse110.library.rules.*;

public class TestEnterpriseComponent {
	
    Member m1, m2;
    Publication book1, book2;
    FacadeInterface geisel;
   
	
	@Before
	public void setUp(){
		m1 = new Member("Mark", MemberType.Teacher);
		m2 = new Member("Nick", MemberType.Teacher);
		
		book1 = new Book("Grapes of Wrath", new RuleObjectBasedLateFeesStrategy());
		book2 = new Book("Hamlet", new RuleObjectBasedLateFeesStrategy());
		
	    factory factory = new factory();
		geisel = factory.make("enterpriseComponent");			
	}
	
	@Test
	public void testCheckoutPublication() throws ParseException {
		geisel.checkoutPublication(m1, book1);
		assertTrue(m1.hasPublication(book1));
	}
	
	
	@Test
	public void testReturnPublication() throws ParseException {
		geisel.checkoutPublication(m1, book1);
		assertTrue(m1.hasPublication(book1));		
		
		 geisel.returnPublication(book1);
			
		    assertFalse(m1.hasPublication(book1));
			
	        assertFalse(book1.isCheckout());
			assertEquals(2.00, m1.getDueFees(), 0.01);
			
			
			assertTrue(geisel.hasFee(m1));
			assertTrue(geisel.getFee(m1) == 2.0);
			
	}
	
	@Test
	public void testGetFee() throws ParseException {
	}


	@Test
	public void testHasFee() throws ParseException {
	}
	
}
